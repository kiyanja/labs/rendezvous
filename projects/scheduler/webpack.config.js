const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const mf = require("@angular-architects/module-federation/webpack");
const path = require("path");
const share = mf.share;

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  path.join(__dirname, '../../tsconfig.json'),
  [/* mapped paths to share */]);

module.exports = {
  output: {
    uniqueName: "scheduler",
    publicPath: "auto",
    scriptType: "text/javascript" //......:BENA
  },
  optimization: {
    runtimeChunk: false
  },   
  resolve: {
    alias: {
      ...sharedMappings.getAliases(),
    }
  },
  experiments: {
    outputModule: true
  },
  plugins: [
    new ModuleFederationPlugin({
        //library: { type: "module" }, //...:BENA removed as not required

        // For remotes (please adjust)
        name: "scheduler",
        filename: "remoteEntry.js",  //.....:BENA
        exposes: {
            './EventListModule': './projects/scheduler/src/app/event-list/event-list.module.ts',
        },        
        
        // For hosts (please adjust)
        // remotes: {
        //     "appointment": "http://localhost:4200/remoteEntry.js",
        //     "resourceManager": "http://localhost:6200/remoteEntry.js",
        //     "serviceManager": "http://localhost:7200/remoteEntry.js",

        // },

        shared: share({
          "@angular/core": { singleton: true, strictVersion: true, requiredVersion: 'auto' }, 
          "@angular/common": { singleton: true, strictVersion: true, requiredVersion: 'auto' }, 
          "@angular/common/http": { singleton: true, strictVersion: true, requiredVersion: 'auto' }, 
          "@angular/router": { singleton: true, strictVersion: true, requiredVersion: 'auto' },

          ...sharedMappings.getDescriptors()
        })
        
    }),
    sharedMappings.getPlugin()
  ],
};
