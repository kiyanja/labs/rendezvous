import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResourceListComponent } from './resource-list/resource-list.component';

const routes: Routes = [
  {path: '', redirectTo: '/resource-list', pathMatch: 'full'},
  {path: 'resource-list', component: ResourceListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
