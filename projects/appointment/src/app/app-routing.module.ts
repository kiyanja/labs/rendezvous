import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { loadRemoteEntry, loadRemoteModule } from '@angular-architects/module-federation';

const SCHEDULER_APP_URL = "http://localhost:5200/remoteEntry.js";
const RESOURCE_MANAGER_APP_URL = "http://localhost:6200/remoteEntry.js"
const SERVICE_MANAGER_APP_URL = "http://localhost:7200/remoteEntry.js"

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {
    path: 'event-list', 
   loadChildren: () => {
    return loadRemoteModule({
      remoteEntry: SCHEDULER_APP_URL,
      remoteName: "scheduler",
      exposedModule: "./EventListModule"
    }).then(m => m.EventListModule).catch(err => console.log(err));
   }
  },
  {
    path: 'resource-list', 
   loadChildren: () => {
    return loadRemoteModule({
      remoteEntry: RESOURCE_MANAGER_APP_URL,
      remoteName: "resourceManager",
      exposedModule: "./ResourceListModule"
    }).then(m => m.ResourceListModule).catch(err => console.log(err));
   }
  },
  {
    path: 'service-list', 
   loadChildren: () => {
    return loadRemoteModule({
      remoteEntry: SERVICE_MANAGER_APP_URL,
      remoteName: "serviceManager",
      exposedModule: "./ServiceListModule"
    }).then(m => m.ServiceListModule).catch(err => console.log(err));
   }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routeCompArr = [HomeComponent]
