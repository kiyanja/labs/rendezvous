const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const mf = require("@angular-architects/module-federation/webpack");
const path = require("path");
const share = mf.share;

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  path.join(__dirname, '../../tsconfig.json'),
  [/* mapped paths to share */]);

module.exports = {
  output: {
    uniqueName: "appointment",
    publicPath: "auto",
    scriptType: "text/javascript"   //....:BENA
  },
  optimization: {
    runtimeChunk: false
  },   
  resolve: {
    alias: {
      ...sharedMappings.getAliases(),
    }
  },
  experiments: {
    outputModule: true
  },
  plugins: [
    new ModuleFederationPlugin({
        //library: { type: "module" },  //Remove as not required...... :BENA

        // For remotes (please adjust)
         name: "appointment", //....:BENA to identfy the consumer
        // filename: "remoteEntry.js",
        // exposes: {
        //     './Component': './projects/appointment/src/app/app.component.ts',
        // },        
        
        // For hosts (please adjust)
         remotes: {
            "scheduler": "scheduler@http://localhost:5200/remoteEntry.js",  //....:BENA adapt the correct script name
            "resourceManager": "resourceManager@http://localhost:6200/remoteEntry.js",
            "serviceManager": "serviceManager@http://localhost:7200/remoteEntry.js",

        },

        shared: share({
          "@angular/core": { singleton: true, strictVersion: true, requiredVersion: 'auto' }, 
          "@angular/common": { singleton: true, strictVersion: true, requiredVersion: 'auto' }, 
          "@angular/common/http": { singleton: true, strictVersion: true, requiredVersion: 'auto' }, 
          "@angular/router": { singleton: true, strictVersion: true, requiredVersion: 'auto' },

          ...sharedMappings.getDescriptors()
        })
        
    }),
    sharedMappings.getPlugin()
  ],
};
